# api_cursos_v2

API PARA ARMAZENAMENTO DE CURSOS DESENVOLVIDA PARA A DISCIPLINA DE "DESENVOLVIMENTO DE SOFTWARES EM CAMADA" NO CENTRO UNIVERSITARIO BELAS ARTES DE SÃO PAULO COM O PROFESSOR FLAVIO VIEIRA.


## Development Info
Api desenvolvida em Python utilizando o FASTAPI.
Api comunicando com banco em Postgres.
Api hospedada no Render.

## Utils
Lembre-se dos requirements.txt
